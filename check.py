import yaml

def fill_missing_fields_with_null(yaml_src, yaml_ref):

    if isinstance(yaml_ref, dict):
        if not isinstance(yaml_src, dict): #if the dict in not present 
            yaml_src = {}
        for key in yaml_ref.keys(): #go through all the keys  
            if key not in yaml_src:
                yaml_src[key] = None #set to null 
            else:
                yaml_src[key] = fill_missing_fields_with_null(yaml_src[key], yaml_ref[key])
    elif isinstance(yaml_ref, list):
        if not isinstance(yaml_src, list):
            yaml_src = []
        while len(yaml_src) < len(yaml_ref):
            yaml_src.append(None)
        for i in range(len(yaml_src)):
            if i < len(yaml_ref):
                yaml_src[i] = fill_missing_fields_with_null(yaml_src[i], yaml_ref[i])
    return yaml_src

def main():
    # Chemins des fichiers YAML
    file1_path = "file1.yaml"
    file2_path = "file2.yaml"
    result_path = "result.yaml"

    # Charger les fichiers YAML
    with open(file1_path, "r") as file1, open(file2_path, "r") as file2:
        data1 = yaml.safe_load(file1) or {}
        data2 = yaml.safe_load(file2) or {}

    # Remplir les champs manquants uniquement avec None
    result = fill_missing_fields_with_null(data1, data2)

    # Enregistrer le résultat dans un fichier YAML
    with open(result_path, "w") as result_file:
        yaml.safe_dump(result, result_file, default_flow_style=False, allow_unicode=True)

    print(f"Résultat enregistré dans {result_path}")

if __name__ == "__main__":
    main()
