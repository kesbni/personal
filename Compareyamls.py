import os
import yaml

def load_yaml_files(directory):
    """Charge tous les fichiers YAML dans un dossier et retourne une liste de dictionnaires."""
    yaml_data = []
    
    for file in os.listdir(directory):
        if file.endswith((".yaml", ".yml")):
            file_path = os.path.join(directory, file)
            with open(file_path, "r", encoding="utf-8") as f:
                data = yaml.safe_load(f) or {}  # Charger avec gestion du cas None
                yaml_data.append(data)
    
    return yaml_data

def find_common_keys(yaml_data):
    """Compare les dictionnaires YAML et retourne un dictionnaire avec les valeurs communes."""
    if not yaml_data:
        return {}

    # Prendre les clés du premier fichier comme référence
    common_keys = yaml_data[0]

    for data in yaml_data[1:]:
        # Filtrer les clés qui existent dans tous les fichiers avec les mêmes valeurs
        common_keys = {k: v for k, v in common_keys.items() if k in data and data[k] == v}

    return common_keys

def save_common_values(common_data, output_file):
    """Enregistre les valeurs communes dans un fichier YAML."""
    with open(output_file, "w", encoding="utf-8") as f:
        yaml.dump(common_data, f, default_flow_style=False, allow_unicode=True)

def main(directory, output_file="common_values.yaml"):
    """Exécute le processus de comparaison et d'extraction des valeurs communes."""
    yaml_data = load_yaml_files(directory)
    common_values = find_common_keys(yaml_data)
    
    if common_values:
        save_common_values(common_values, output_file)
        print(f"Fichier généré : {output_file}")
    else:
        print("Aucune valeur commune trouvée.")

# Modifier le chemin du dossier ici
directory_path = "chemin/vers/votre/dossier"
main(directory_path)