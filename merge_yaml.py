import yaml
import sys

def merge_yaml_with_nulls(file1, file2, output_file):
    try:
        # Charger les fichiers YAML
        with open(file1, 'r') as f1:
            yaml1 = yaml.safe_load(f1) or {}

        with open(file2, 'r') as f2:
            yaml2 = yaml.safe_load(f2) or {}

        # Fonction récursive pour ajouter les champs manquants avec des valeurs nulles
        def add_missing_fields(source, target):
            if isinstance(source, dict):
                for key, value in source.items():
                    if key not in target:
                        target[key] = None if not isinstance(value, dict) else {}
                    if isinstance(value, dict):
                        add_missing_fields(value, target[key])

        add_missing_fields(yaml1, yaml2)

        # Sauvegarder le fichier de sortie
        with open(output_file, 'w') as out:
            yaml.dump(yaml2, out, default_flow_style=False, allow_unicode=True)

        print(f"Fichier de sortie sauvegardé : {output_file}")

    except Exception as e:
        print(f"Erreur lors du traitement des fichiers : {e}")

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage : python script.py <fichier1.yaml> <fichier2.yaml> <output.yaml>")
    else:
        file1 = sys.argv[1]
        file2 = sys.argv[2]
        output_file = sys.argv[3]
        merge_yaml_with_nulls(file1, file2, output_file)
